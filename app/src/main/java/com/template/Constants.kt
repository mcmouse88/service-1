package com.template

object Constants {
    const val COLLECTION_ID = "database"
    const val DOCUMENT_ID = "check"
    const val FIELD = "link"
    const val KEY_LINK = "field_link"
    const val TIME_OUT = 2_000L
    const val LONG_TIME_OUT = 4_000L
    const val FIRST_LAUNCH = "first_launch"
    const val SAVED = "saved"
}
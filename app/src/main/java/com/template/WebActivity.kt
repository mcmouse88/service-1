package com.template

import android.annotation.SuppressLint
import android.content.pm.ActivityInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.webkit.WebView
import com.template.Constants.KEY_LINK
import java.util.*

class WebActivity : AppCompatActivity() {

    private var link: String? = null

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR
        parseIntent()
        val webView = findViewById<WebView>(R.id.webView)
        webView.settings.javaScriptEnabled = true
        webView.loadUrl(createUrl())
        val url = createUrl()
        Log.d("checkUrl", url)
    }

    private fun parseIntent() {
        if(!intent.hasExtra(KEY_LINK)) {
            throw RuntimeException("args in WebActivity  is absent")
        }
        link = intent.getStringExtra(KEY_LINK)
        Log.d("checkIntent", "$link")
    }

    private fun createUrl(): String {
        val builder = StringBuilder()
        val uuid = UUID.randomUUID()
        val timeZone = TimeZone.getDefault().id
        builder.append(link)
            .append("/?packageid=")
            .append(applicationContext.packageName)
            .append("&usserid=")
            .append(uuid)
            .append("&getz=")
            .append(timeZone)
            .append("&getr=utm_source=google-play&utm_medium=organic")
        return builder.toString()
    }
}
package com.template

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.template.Constants.COLLECTION_ID
import com.template.Constants.DOCUMENT_ID
import com.template.Constants.FIELD
import com.template.Constants.FIRST_LAUNCH
import com.template.Constants.KEY_LINK
import com.template.Constants.LONG_TIME_OUT
import com.template.Constants.SAVED
import com.template.Constants.TIME_OUT
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class LoadingActivity : AppCompatActivity() {

    private val db = Firebase.firestore
    val analytics = Firebase.analytics
    private val prefs by lazy {
        getSharedPreferences("com.template.server_v1", MODE_PRIVATE)
    }
    private var constLink: String? = null

    @SuppressLint("SourceLockedOrientationActivity")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loading)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        if (!isNetworkConnected()) {
            lifecycleScope.launch {
                delay(LONG_TIME_OUT)
                goToRightActivity(MainActivity::class.java)
            }
            showToast()
        } else {
            checkLink()
            getLink()
        }
    }

    private fun goToRightActivity(cls: Class<*>, link: String? = null) {
        val intent = Intent(this@LoadingActivity, cls)
        if (link != null) {
            intent.putExtra(KEY_LINK, link)
        }
        startActivity(intent)
        finish()
    }

    private fun checkLink() {
        if (prefs.getBoolean(FIRST_LAUNCH, true)) {
            Log.d("check_request", "request")
            val docRef = db.collection(COLLECTION_ID).document(DOCUMENT_ID)
            docRef.get()
                .addOnSuccessListener { document ->
                    if (document != null) {
                        if (document.data != null) {
                            prefs.edit()
                                .putString(SAVED, document.data?.get(FIELD).toString())
                                .apply()
                        }
                    }
                }
            prefs.edit().putBoolean(FIRST_LAUNCH, false).apply()
        }
    }

    private fun getLink() {
        lifecycleScope.launch {
            delay(TIME_OUT)
            constLink = prefs.getString(SAVED, null)
            if (constLink != null) {
                goToRightActivity(WebActivity::class.java, constLink)
            } else goToRightActivity(MainActivity::class.java)
        }
    }

    private fun isNetworkConnected(): Boolean {
        val connectManager =
            getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val network = connectManager.activeNetwork
            network ?: return false
            val netWorkCapabilities = connectManager.getNetworkCapabilities(network) ?: return false
            return when {
                netWorkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true

                netWorkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                else -> false
            }
        }
        return false
    }

    private fun showToast() {
        val toast = Toast.makeText(this, R.string.check_connection, Toast.LENGTH_LONG)
        toast.setGravity(Gravity.TOP, 10, 10)
        toast.show()
    }
}
